% Unit Management Toolbox
% Version 1.0   15-Dec-1998
%
% Created by: Douglas Harriman
%
% 
%  base              - Converts unit object to base units.
%  reduce            - Reduce unit object by cancelling units.
%
% Unit library managment functions. 
%  defineunit        - Define a new unit object.
%  deletefromlibrary - Deletes a unit definition from the Unit Library.
%  regenlibrary      - Regenerates basic Unit Library.
%  u                 - Quick unit object definition function.
%  units             - List defined units.
%
% General Purpose Functions.
%  findrun           - Finds runs of 1's in boolean vectors.
%  isbool            - Determines whether a matrix is boolean or not.
%  isnum             - Determines whether characters are numeric or not.
%
% Unit Object Functions
%  unit    - Unit class constructor.
%  display - Command line display function.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

