%CONVERT Converts units of number to requested units.
%
%>> unitobj = convert(val,orig_unit,new_unit)
%>> convert VAL ORIG_UNIT NEW_UNIT
%

% Doug Harriman (dharriman@xzeres.com)

function out = convert(val,from,to)

% Error checks
narginchk(2,3);

% Attempt numeric conversion of the val
if ischar(val)
    try
        val = str2num(val);
    catch
        error('Must provide a numeric value for conversion.');
    end
end

% Input checks
assert(isnumeric(val));
assert(ischar(from));
assert(ischar(to));

% Create a unit object
obj = unit(val,from);

out = convert(obj,to);
