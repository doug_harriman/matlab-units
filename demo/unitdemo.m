%UNITDEMO

% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>
% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>

echo on
clc

% This file demonstrates the unit toolbox.
% 
% Lets start by defining two distances.
% Unit objects are defined by calling the UNIT 
% command:
%
d1 = unit(1,'in')

pause
clc
% 
% Unit objects can also be created on the command
% line with a shorthand command U:
%
u d2 = 40 mm

pause
clc

%
% Most standard Matlab commands have been overloaded to
% work with unit objects.  Operations such as '+' and '-' will
% return an error if the operands are of incompatible units,
% while operators such as '*' and '/' will combine the units
% appropriately.
%
% To illustrate, we will add the length and convert to 'cm'.
%
d3 = d1 + d2
convert(d3,'cm')
d3

pause
clc

% 
% We can also create an area:
%
a1 = d1*d2
convert(a1,'mm^2')
a1

pause
clc

% 
% Now lets complete a little exercise.  Assume we have a spring:
%
u k = 2.5 N/m

pause
clc

% 
% And it gets stretched by distance d1.  What force does it generate?
%
f = k*d1
f = reduce(f)


pause
clc

%
% Finally, what pressure does this force exert when applied over
% area a1?
%
p = f/a1
convert(p,'pa')
p


pause
clc

%
% We can add and remove units from the library.  Let's remove the
% definitions of inches, then add it back.
% First we remove it:
%
deletefromlibrary('in')


pause
clc

%
% Now we add it back.  First we define a unit object with a length
% equivalent to 1 in.  There are 39.37 inches per meter, so:
%
equiv_obj = unit(1/39.37,'m')

%
%
% The next step is to associate a new symbol and name with the 
% equivalent object.  This step will do the association, and 
% add the new definition to the unit library.
%
defineunit(equiv_obj, 'in', 'inches')


pause
clc

% 
% Now a quick check of our new unit.
%
u x = 1 m
convert(x,'in');
x


% That's it!
echo off