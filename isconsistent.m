%ISCONSISTENT  Checks for consistancy of units.
%
%>>isConsistentBoolean = isconsistent(unitObj1, unitObj2)
%

% Doug Harriman
% 05-Oct-2010

function iscon = isconsistent(u1,u2)

% If have strings, create units.
if ischar(u1)
    u1 = unit(1,u1);
end
if ischar(u2)
    u2 = unit(1,u2);
end

% Handle Channels
if ischannel(u1)
    u1 = unit(1,units(u1));
end
if ischannel(u2)
    u2 = unit(1,units(u2));
end

% If neither are units, then they are consistent.
if ~isa(u1,'unit') && ~isa(u2,'unit')
    iscon = true;
else
    iscon = isconsistent(u1,u2);
end