%ISBOOL   Returns true is a matrix is boolean.
%

% DLH, 11/29/98

% TODO - Convert to using a more direct boolean check.

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function val = isbool(in)

% Find any nonboolean entries.
val = (in~=0) & (in~=1);

% Make sure none exist.
if abs(max(val))>0
   val = 0;
else 
   val = 1;
end
