%REGENLIBRARY  Deletes and regenerates default unit library.
%
%>> regenlibrary
%

% DLH
% 11/27/98 - Created
% 09/12/02 - Added several units (cm,mm,um,nm,A).

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [] = regenlibrary()

% Make sure library exists
if exist('library.mat','file'),
   % Delete it
   delete('library.mat');
end

% Add the base units
addtolibrary('m',  1,[1 0 0 0 0],'meters');
addtolibrary('g',  1,[0 1 0 0 0],'grams');
addtolibrary('sec',1,[0 0 1 0 0],'seconds');
addtolibrary('C',  1,[0 0 0 1 0],'Coulombs');
addtolibrary('rad',1,[0 0 0 0 1],'radians');

% Add standard length units
addtolibrary('km', 1000,    [1 0 0 0 0],'kilometers');
addtolibrary('cm', 1/100,   [1 0 0 0 0],'centimeters');
addtolibrary('mm', 1/1000,  [1 0 0 0 0],'millimeters');
addtolibrary('um', 1e-6,    [1 0 0 0 0],'micrometers');
addtolibrary('nm', 1e-9,    [1 0 0 0 0],'nanometers');
addtolibrary('in', 1/39.37, [1 0 0 0 0],'inches');
addtolibrary('ft', 12/39.37,[1 0 0 0 0],'feet');
%addtolibrary('',  1,[1 0 0 0 0],'');

% Add standard mass units
addtolibrary('kg', 1000,[0 1 0 0 0],'kilograms');
%addtolibrary('',  1,[0 1 0 0 0],'');

% Add standard time units
addtolibrary('ms', 1/1000,[0 0 1 0 0],'milliseconds');
addtolibrary('hr', 3600,  [0 0 1 0 0],'hours');
addtolibrary('min',60,    [0 0 1 0 0],'minutes');
%addtolibrary('', 1,[0 0 1 0 0],'');

% Add standard angle units
addtolibrary('deg', pi/180,[0 0 0 0 1],'degrees');

% Add some standard compound units.
addtolibrary('N',   1000,[1 1 -2 0 0],    'Newtons');
addtolibrary('A',   1,   [0 0 -1 1 0],    'Amps');
addtolibrary('pa',  1000,[-1 1 -2 0 0],   'Pascals');
