%ADDTOLIBRARY  Adds a unit definition to the unit database.
%
%>>addtolibrary(symbol,baseFactor,baseExponent,longName)
%

% DLH
% 08/19/02 - Handles regenerating with missing MAT file better.
% 11/27/98 - Created

function []=addtolibrary(symbol,baseFactor,baseExponent,longName)

% Make sure library exists
if exist('library.mat','file'),
    % Load database
    load library
    noFileFlag = 0;
else
    % Create the empty data file
    path = fileparts(which(mfilename));
    fileName = [path filesep 'library.mat'];
end

% Error check the symbol

% Error check the factor

% Error check the exponent

% See if we have a longName
if nargin == 4,
    % Error check long name
    if ~ischar(longName)
        error('Long unit name must be a string.');      
    end
    if size(longName,1)>1
        error('Long unit name must be a single line.'); 
    end
else
    longName = ' ';
end

% Add data to end of library
library.(symbol).data = [baseFactor baseExponent];
library.(symbol).name = longName;

% Store time it was saved
libSaveTime = datestr(now,0);

% Resave library to it's path
filePath = which('library.mat');
save(filePath,'library');

% Force lookup to reload file.
clear unit/private/lookup

% EOF