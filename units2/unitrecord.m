%UNITRECORD  Unit database record.
%

%<AUTHOR>
% Doug Harriman (doug.harriman@gmail.com)
%</AUTHOR>

classdef unitrecord
    properties
        % Unit name
        name

        % UCUM input code, case sensitive.
        Code
        
        % UCUM input code, case insensitive
        CODE
        
        % User defined name alias.
        alias = ''
        
        % Command line output symbol.
        printsymbol = ''
        
        % Latex output symbol.
        latexsymbol = ''
        
        % Is base unit?
        isbase = false
        
        % Function to convert to base units.
        tobase
        
        % Function to convert from base units.
        frombase
        
        % Dimension structure.
        dimensions
        
        % Metric unit
        ismetric
        
        % Unit class
        class
        
    end % properties
    
    methods
        function obj = unitrecord(varargin)
            %UNITRECORD  Constructor.
            %
            
            
        end % constructor
        
        function obj = set.isbase(obj,value)
            
            % Error checks
            error(nargchk(2,2,nargin));
            if ~isbool(value)
                error('Boolean value expected.');
            end
            
            % Assign value.
            obj.isbase = value;
            
            % If setting to base, force conversion functions to essentially
            % a null op.
            obj.tobase = @(val)val;
            obj.frombase = @(val)val;
            
        end % isbase
        
    end % methods
    
end % classdef