%UNITXMLREAD

function [out] = unitxmlread(filename)

if nargin == 0
    filename = 'ucum-essence.xml';
end
doc  = xmlread(filename);

% Read in different data types
list = doc.getElementsByTagName('base-unit');
out.base_units = DOMList2StructArray(list);

list = doc.getElementsByTagName('prefix');
out.prefixes = DOMList2StructArray(list);

list = doc.getElementsByTagName('unit');
out.units = DOMList2StructArray(list);


% Convert a list of DOM objects to an array of structures.
function [S] = DOMList2StructArray(list)

idx = 1;
for i = 0:list.getLength-1
    child_node = list.item(i);

    % Get child elements
    if ~isempty(child_node) && (child_node.getNodeType == child_node.ELEMENT_NODE)
        S(idx) = DOM2Struct(child_node);
        idx = idx + 1;
    end
end


% Convert a DOM representation to a struture.
% All atttributes and child elements are converted to structure fields.
function [S] = DOM2Struct(node)

% Get default structure based on node type
S = GetDefaultStruct(char(node.getTagName));

% Convert attributes to data field first
attr_list = node.getAttributes;
for i = 0:attr_list.getLength - 1
    attr = attr_list.item(i);
    tag  = char(attr.getNodeName);
    data = char(attr.getNodeValue);
    S.(tag) = data;
end

child = node.getFirstChild;
while ~isempty(child)
    % Convert child nodes to data fields.
    if child.getNodeType == child.ELEMENT_NODE
        % Read the data
        tag  = char(child.getTagName);
        
        % Special handling of the value field.
        % Don't want the contents of the field, want its attributes.
        if strcmp(tag,'value')
            attr_list = child.getAttributes;
            for i = 0:attr_list.getLength - 1
                attr = attr_list.item(i);
                tag  = char(attr.getNodeName);
                data = char(attr.getNodeValue);
                S.(tag) = data;
            end
            
        else
            data = char(child.getTextContent);
            S.(tag) = data;
        end
    end
    
    % Get next node
    child = child.getNextSibling;
end


% Generate default structures.
function [S] = GetDefaultStruct(type)

S = struct();

switch(type)
    case 'base-unit';
        S.CODE = [];
        S.Code = [];
        S.dim  = [];
        S.name = [];
        S.printSymbol = [];
        S.property    = [];
        
    case 'prefix'
        S.CODE = [];
        S.Code = [];
        S.name = [];
        S.printSymbol = [];
        S.value = [];
        
    case 'unit'
        S.CODE        = [];
        S.Code        = [];
        S.isMetric    = [];
        S.class       = [];
        S.dim         = [];
        S.name        = [];
        S.printSymbol = [];
        S.property    = [];
        S.value       = [];
        S.Unit        = [];
        S.UNIT        = [];
end
    