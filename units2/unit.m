%UNIT  Unit object class.

%<AUTHOR>
% Doug Harriman (doug.harriman@gmail.com)
%</AUTHOR>

%TODO - If need to access dimensions & units can do props that use static
%       methods to get correct property behavior using static methods with
%       persistent data.

% Design:
% Store both dimensions and units in structures.  Whenever we add new
% fields, sort fields with ORDERFIELDS.  Combined with STRUCT2CELL, can
% mimic behavior of an ordered vector like is used in the original UNITS
% implementation.
% Whenever we load a new value, need to make sure that we create a union of
% the fields so that everything stays aligned.

% Must keep unit object's dimension lists in sync with master list at all
% times.  
% How to do efficiently?
% - Check dimension fields every time.
% - Track some sort of version number of the dimension db when the obj is
%   created.  Only have to update or sync if versions are different.  

% To add a new dimension, must define a new unit that is a base unit for
% that dimension.

classdef unit
    properties (Transient=true, SetAccess=private)
        % Defined unit dimensions, static.
        dimensions
        
        % Dimensions version, static
        dimensions_version
        
        % Unit array for this object
        unitvec
        
    end % properties - dependant, set private
    
    properties (SetAccess=private)
        
    end % properties - set private
    
    properties (Constant=true)
        % Unit class version
        version = 2;

    end % properties - constant
    
    methods
        function obj = unit(varargin)
            %UNIT  Unit object constructor.
            %
            
        end % constructor
        
        function obj = set.dimensions(obj,val)
            %SET.DIMENSIONS  Set dimensions database.
            %
            
            %TODO - Error checking
            unit.setgetdims(val);
            
        end % set.dimensions
        
        function val = get.dimensions(varargin)
            %GET.DIMENSIONS  Get dimensions database.
            %
            
            val = unit.setgetdims;
            
        end % get.dimensions
        
    end % methods - public

    methods (Static) %, Access=private)
        function val = setgetdims(val)
            %SETGETDIM  Set or get the dimensions array.
            %
        
            persistent dims
            
            switch nargin
                case 0
                    if isempty(dims)
                        dims = {};
                    end
                    
                    val = dims;
                case 1
                    % Error check
                    assert(iscellstr(val));
                    
                    % Set
                    dims = val;
                    
                otherwise 
                    error('Zero or one inputs expected.');
            end
        end % setgetdims

        function val = setgetdimsver(val)
            %SETGETDIMSVER  Set or get dimensions structure version.
            %
            
            persistent version
            
            switch(nargin)
                case 0
                    % Get
                    if isempty(version)
                        version = 1;
                    end
                    val = version;
                    
                case 1 
                    % Error check
                    assert(isnumeric(val));
                    assert(val > version);
                    
                    % Set
                    version = val;
                    
                otherwise
                    error('Zero or on inputs expected.');
            end
                    
        end % setgetdimsver
            
        function val = setgetunitdb(val)
            persistent unitdb
            
            switch nargin
                case 0
                    % Get
                    val = unitdb;
                case 1
                    % Set
                    unitdb = val;
                otherwise 
                    error('Zero or one inputs expected.');
            end
            
        end % setgetunitdb
        
        function addrecord(rec)
            %ADDRECORD  Add unit database record.
            %
            
            % Error checks
            error(nargchk(1,1,nargin))
            if ~isa(rec,'unitrecord')
                error('UNITRECORD object expected.');
            end
            
            % Make sure that all dimensions used in the unit are supported.
            dims = unit.setgetdims;
            dims_used = fieldnames(rec.dimensions);
            if ~all(ismember(dims_used,dims))
                % Have one or more unsupported dimensions.
                if length(dims_used) > 1
                    % Have a composite unit with one or more unsupported
                    % dimenion.
                    tf = ismember(dims_used,dims);
                    idx = find(tf);
                    idx = idx(1);
                    error(['Unsupported dimension: ' dims_used{idx}]);
                    
                else
                    % Have a single dimenioned unit that is undefined.  If
                    % this is a base unit, OK.  Otherwise error.
                    if ~rec.isbase
                        error(['Dimension unsupported.  Add base unit first: ' dims_used{1}]);
                    else
                        % Have a base unit for a new dimension.
                        % Update the dimension list and version.
                        dims{end+1,1} = dims_used{1};
                        dims = sort(dims);
                        unit.setgetdims(dims);
                        ver = unit.setgetdimsver;
                        ver = ver + 1;
                        unit.setgetdimsver(ver);
                        
                    end
                end
            end
            
            % Default to storing by case insensitive name.
            field = lower(rec.CODE);
            unitdb = unit.setgetunitdb;
            unitdb.(field) = rec;
            unit.setgetunitdb(unitdb);
            
        end % addrecord
        
        function [] = parse
            %PARSE  Parse XML unit database.
            %
            
            % Create some basic units.
            % meters.
            val = unitrecord;
            val.name = 'meter';
            val.Code = 'm';
            val.CODE = 'M';
            val.isbase = true;
            val.dimensions.length = 1;
            unit.addrecord(val);
            
            % inches
            val = unitrecord;
            val.name     = 'inch';
            val.Code     = 'in';
            val.CODE     = 'IN';
            val.tobase   = @(val)val/39.37;  % in -> m
            val.frombase = @(val)val*39.37;  % m -> in 
            val.dimensions.length = 1;
            unit.addrecord(val);
            
            % Kelvin
            val = unitrecord;
            val.name   = 'Kelvin';
            val.Code   = 'K';
            val.CODE   = 'K';
            val.isbase = true;
            val.dimensions.temperature = 1;
            unit.addrecord(val);

            % Degrees Centrigrade
            val = unitrecord;
            val.name = 'Degrees Celsius';
            val.Code = 'DegC';
            val.CODE = 'DEGC';
            val.tobase = @(val) val + 273.15;
            val.dimensions.temperature = 1;
            val.printsymbol = [char(176) 'C'];
            val.latexsymbol = '\circC';
            unit.addrecord(val);
            
            % Degrees Fahrenheit
            val = unitrecord;
            val.name = 'Degrees Fahrenheit';
            val.Code = 'DegF';
            val.CODE = 'DEGF';
            val.tobase = @(val) 9*val/5 + 273.15;
            val.dimensions.temperature = 1;
            val.printsymbol = [char(176) 'F'];
            val.latexsymbol = '\circF';
            unit.addrecord(val);
            
            % Seconds
            val = unitrecord;
            val.name = 'seconds';
            val.Code = 'sec';
            val.CODE = 'SEC';
            val.dimensions.time = 1;
            val.isbase = true;
            unit.addrecord(val);
            
            % Minutes
            val = unitrecord;
            val.name = 'Minutes';
            val.Code = 'min';
            val.CODE = 'MIN';
            val.dimensions.time = 1;
            val.tobase = @(val)val*60;
            val.frombase = @(val)val/60;
            unit.addrecord(val);
            
            % Hertz
            val = unitrecord;
            val.name = 'Hertz';
            val.Code = 'Hertz';
            val.CODE = 'HERTZ';
            val.dimensions.time = -1;
            val.tobase = @(val)1/val;
            val.frombase = @(val)1/val;
            unit.addrecord(val);
            
        end % parse
        
    end % methods - static, private
end % classdef