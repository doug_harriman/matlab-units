%PARSE  Parse a unit string.


% Support two modes
mode = 'matlab';  % Normal Matlab symbols and syntax
mode = 'ucum';    % UCUM symbols and syntax

% UCUM mode
% "." - Multiply
% "*" - Power of 10 Exponent
% ""  - Exponent.  Ex m2 => m^2
% "/" - Divide.  Only affects following symbol.
