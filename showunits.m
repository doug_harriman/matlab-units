%UNITS  List of defined units and their symbols.
%

% DLH, 12/12/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [] = units()

% Get the list
load UnitLibrary;

% Build the strings
divider = char(ones(size(nameLib,1),1)*[' = ']);

% List it
disp('Defined Units:');
disp([symbolLib divider nameLib]);