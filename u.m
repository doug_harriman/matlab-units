%U   Unit object constructor, short version.
%    U allows the user to create a unit object with 
%    a single character command.
%
% >> u VARIABLE = VALUE UNIT_STRING
% 
%Example:
% >> u x = 12 m     This creates a unit object named x equal to 12 meters.
%
%Warning: The unit string must be preceded by whitespace, but should not contain
%         any whitespace.
%
% See also UNIT.
%

% DLH 11/22/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function unitobj = u(varargin)

% Min of 1, max of 4 arguements.  May have to parse the VARIABLE, '=',
% and VALUE apart.
if nargin == 4,
   
   varName = varargin{1};
   value   = varargin{3};
   unitStr = varargin{4};
   
elseif nargin == 1,
	error('Not enought arguements.  The unit string must be preceded by whitespace.');   
   
else
   
   % Combine into one string then parse that.
   str = '';
   for i = 1:nargin-1,
   	str = [str varargin{i}];
   end
   
   % Get the unitStr
   unitStr = varargin{nargin};
   
   % Parse the string
   [varName, value] = parse(str);
   
end

% Convert value from string to double.
temp = str2num(value);
if isempty(temp),
   % If user passed a variable instead of a value, we got nothing back.
	% Need to do a trick to get the right value.
   value = evalin('caller',value);
else,
   value = temp;
end

% Create unit object
unitObj = unit(value,unitStr);

% Assign the value in the caller space
assignin('caller',varName,unitObj);

% Search last input argument for ';'
if isempty(findstr(varargin{nargin},';')),
   evalin('base',varName);
end


%-------------------------------------------------------------------
% Local functions
%-------------------------------------------------------------------

function [varName, valueStr] = parse(str)

% Need to separate everything apart, error checking as we go.
eqSignInd = findstr(str,'=');
if isempty(eqSignInd), error('Illegal syntax: ''='' required.'); end

% Pull out he variable name
varName  = str(1:eqSignInd-1);
valueStr = str(eqSignInd+1:end);