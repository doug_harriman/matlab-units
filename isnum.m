%ISNUM True for numeric characters.
%      For a string S, ISNUM(S) is 1 for numeric characters
%      and 0 otherwise.
 
% DLH 12/19/98
 
% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [out] = isnum(str)
 
 % Convert to ASCII values
 asciiVal = double(str);
 
 % Convert to boolean
 out = (asciiVal>=48) & (asciiVal<=57) | (asciiVal==46);