%UNIT/LENGTH  Length of unit object vector.
%
%   See also length.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = length(obj)

if nargin > 1
    error('Too many input arguments.');
end
if nargout > 1 
    error('Too many output arguments.');
end

% Return the value length
out = length(obj.value);