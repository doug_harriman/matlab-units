%UNIT/DOUBLE  Convert a unit object to a unitless value.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function num = double(unitobj)

% Loop through unit definitions
for i = 1:length(unitobj.unit),
   % Change the value
   unitobj.value = unitobj.value/prod(unitobj.unit(i).baseFactor^unitobj.unit(i).symbolExponent) ;
end

% Set up output
num = unitobj.value;

