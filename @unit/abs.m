%UNIT/ABS   Absolute value for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = abs(in)

out = in;
out.value = abs(in.value);