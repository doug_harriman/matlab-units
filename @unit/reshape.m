%UNIT/RESHAPE Reshape a unit object.
%
%  See also: reshape.
%

function [obj] = reshape(obj,varargin)

obj.value = reshape(obj.value,varargin{:});