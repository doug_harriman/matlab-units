%BASE    Reduce the units down to base units.
%        Unit will be expressed in terms of the 
%        following units:
%
%        Length:  meters  'm'
%        Mass  :  grams   'g'
%        Time  :  seconds 'sec'
%        Charge:  Coulomb 'C'
%        Angle :  Radian  'rad'
%

% DLH, 12/19/98
% 01/29/99, Fixed charge base unit.

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [out] = base(in)

% Convert 'in' to a struct
tmp = in;
in = struct(in);

% Handle null unit objects
if isempty(in.unit),
    out = tmp;
    return;
end

% Loop through the units definitions adding up powers
totalExponent = zeros(1,length(in.unit(1).baseExponent));
for i = 1:length(in.unit),
   % Multiply symbolExponent by the baseExponents and accumulate
   totalExponent = totalExponent + in.unit(i).symbolExponent * in.unit(i).baseExponent;
end

% Build out with base units
out =       unit(1,'m')  ^totalExponent(1);
out = out * unit(1,'g')  ^totalExponent(2);
out = out * unit(1,'sec')^totalExponent(3);
out = out * unit(1,'C')  ^totalExponent(4);
out = out * unit(1,'rad')^totalExponent(5);

% Set the right value
out = out * in.value;