%UNIT/TRANSPOSE  Complex conjugate transpose a unit object.
%

function [obj] = ctranspose(obj)

obj.value = obj.value';