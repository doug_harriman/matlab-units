%UNIT/DISP   Display unit object.
%   DISP(OBJ) displays the unit object without printing the variable name.
%
%   See also unit/display, unit/num2str, unit/char.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [] = disp(obj)

% Error checks
if nargin > 1
    error('Too many input arguments.');
end
if nargout > 0
    error('Too many output arguments.');
end

% Convert to a string & display it
if length(obj) > 1
    disp(double(obj));
    disp(char(obj));
else
    disp(num2str(obj));
end
