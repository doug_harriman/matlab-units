%UNIT/FLIPUD  Flip unit vector vertically.
%

% DLH
% 14-Jun-2011

function out = flipud(in,varargin)

% Pass to method for doubles.
out = in;
out.value = flipud(in.value,varargin{:});