%UNIT/FLIPLR  Flip unit vector horizontally.
%

% DLH
% 14-Jun-2011

function out = fliplr(in,varargin)

% Pass to method for doubles.
out = in;
out.value = fliplr(in.value,varargin{:});