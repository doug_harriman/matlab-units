%ROUNDUP  Rounds the value up to the specified value.
%  ROUNDVAL=ROUNDUP(VALUE,PLACE) rounds the given value to the specified
%  PLACE.  
%
%  PLACE may be any value.  
%  Examples:
%  >> roundup(12.6,0.25) -> 12.75
%  >> roundup(0.33,1)    -> 1
%  >> roundup(0.33,5)    -> 5
%

function out = roundup(val,place)

% Error checks
error(nargchk(2,2,nargin));
if ~isa(place,'unit')
    error('PLACE must be specfied as a unit value.');
end

% Convert the val to the units of place.
val  = convert(val,place);

% Store out and convert to double.
ustr = char(val);
val = double(val);
place = double(place);

% Do the rounding and package back up.
out = roundup(val,place);
out = unit(out,ustr);