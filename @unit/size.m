%UNIT/SIZE   Size of a unit object array.
%
%   See also size.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [varargout] = size(obj,dim)

% Pass the right call to size
switch (nargin)
    case (1)
        sz = size(obj.value);
    case(2)
        sz = size(obj.value,dim);
    otherwise
        error('Wrong number of inputs.');
end

% Set up correct outputs
switch (nargout)
    case (1)
        varargout{1} = sz;
    case (0)
        assignin('base','ans',sz);
        evalin('base','ans');
    otherwise
        for i = 1:nargout
            varargout{i} = sz(i);
        end
end