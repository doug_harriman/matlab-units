%UNIT/RDIVIDE   Unit object array right divide.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = rdivide(in1,in2)

% Just use power and multiply
out = in1 .* power(in2,-1) ;