%UNIT/SUM  Sum for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = sum(in,varargin)

% Just diff the value.
out = in;
out.value = sum(in.value,varargin{:});