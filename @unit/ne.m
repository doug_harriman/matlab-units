%UNIT/NE    Not equal check for unit objects
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = ne(in1,in2)

% Return the opposite of EQ
out = ~(in1 == in2);