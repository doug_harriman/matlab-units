%UNIT/UNWRAP  Phase angle unwrapping for unit objects.
%

function out = unwrap(in,varargin)

ustr = char(in);
in   = convert(in,'rad');
data = unwrap(double(in),varargin{:});
data = data - fix(data(1)/pi)*pi;
out  = unit(data,'rad');
out  = convert(out,ustr);
