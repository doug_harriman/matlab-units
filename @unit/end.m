%END  Vector end operator for unit object.
%

function idx = end(obj,k,n)

idx = size(obj);
idx = idx(k);
