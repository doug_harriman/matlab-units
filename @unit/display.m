%UNIT/DISPLAY  Displays a unit object.
%   DISPLAY(OBJ) displays the unit object.
%
%   See also unit/disp, unit/num2str, unit/char.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [] = display(obj)

% Error checks
if nargin > 1
    error('Too many input arguments.');
end
if nargout > 0
    error('Too many output arguments.');
end

% Show input variable name 
disp(' ');
disp([inputname(1) ' =']);
disp(' ');
if ~isempty(obj)
    disp(obj);
else
    disp('empty');
end
    
disp(' ');
