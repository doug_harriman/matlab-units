%UNIT/VERTCAT  Vertical concatination of unit objects.
%
%   See also vertcat.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% TODO - Should support concatenation of dissimilar units.

function [out] = vertcat(varargin)

% Make sure all are unit objects
for i = 1:nargin
    if ~isa(varargin{i},'unit')
        error('Objects must be of type unit.');
    end
end

% Check consistancy
for i = 2:nargin
    if ~isconsistent(varargin{1},varargin{i})
        error('Unit object concatination requires consistent units.');
    end
end

% Concat the values
out = varargin{1};
cols = size(out,2);
for i = 2:nargin
    if size(varargin{i}.value,2) ~= cols
        error('All inputs must have same number of cols.');
    end

    out.value = [out.value; varargin{i}.value];
end
