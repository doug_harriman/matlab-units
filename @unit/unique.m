%UNIQUE Unique method for unit object.
%  See HELP UNIQUE.

% <COPYRIGHT>
% Copyright (c) 1998-2011  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [y,m,n] = unique(x,varargin)

% Copy output over.
y = x;

% Call unique
[y_data,m,n] = unique(x.value,varargin{:});

% Repackage
y.value = y_data;
