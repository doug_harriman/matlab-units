%UNIT/PRIVATE/LOOKUP   Lookup unit definition in database.
%
%>>[baseFactor, baseExponent] = lookup(symbol)
%

% DLH, 11/27/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>

% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [baseFactor, baseExponent] = lookup(symbol)

% Persistant variables
persistent library

% See if we have the libraries loaded
if isempty(library)
    if ~exist('library.mat','file'),
        regenlibrary;
    end
    load library
end

% Get Data
try
    baseFactor   = library.(symbol).data(1);
    baseExponent = library.(symbol).data(2:6);
catch
    error(['Symbol ''' symbol ''' not found in unit library.']);
end