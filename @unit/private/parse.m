%UNIT/PARSE   Parses unit string for defined units.
% PARENTHESIS ARE NOT HANDLED AT THIS TIME.
% >> unitDefList = parse(string)
%

% This thing does not work so great yet.  Exponent handling sucks, and
% parenthesis are not supported.  Must do these well before it is ready
% to release.

% DLH
% 02/10/99 - Fixed error with decimal exponents.
% 11/27/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = parse(str)

% Remove all spaces from string
spaceInd = findstr(str,' ');
str(spaceInd) = '';

% User just passed symbol, need to parse for multiple units
% Characters to look for: * / ^ ( ) 
multind = findstr(str,'*') ;
divind  = findstr(str,'/') ;
carind  = findstr(str,'^') ;
oppind  = findstr(str,'(') ;
clpind  = findstr(str,')') ;
opind   = sort([multind divind carind]) ;  % indecies of all operators
[numStart,numEnd]  = findrun(isnum(str));  % Numeric character indecies.

% DON'T SUPPORT PARENTHESIS
if ~isempty(oppind) || ~isempty(clpind),
   error('UNIT/PRIVATE/PARSE Does not currently support parenthesis.')
end

% Error check number of divide operators
if length(divind) > 1,
   error('Unit expressions may only contain one ''/'' operator.');
end

% Error check for illegal characters, ie non alphanumeric, non operator
charind = find(isletter(str))       ;  % index of all alphanumeric chars
chkstr  = str                       ;  % temp copy of str for checking
chkstr([opind oppind clpind charind]) = '' ;  % delete all legal non numerical chars 
%badind  = find(double(chkstr)<48 | double(chkstr) > 57) ;
badind  = find(double(chkstr)<46 | double(chkstr) > 57) ;  % Allows decimal point, 02/10/99

if ~isempty(badind),
   
   % Have illegal charaters
   disp(' ')
   fprintf('Error: Illegal characters in unit string\n')
   fprintf('Unit string: %s\n',str) ;
   fprintf('Illegal characters: ''%s''\n',chkstr(badind)) ;
   error(' ')
   
end

% Error check parenthesis to make sure all have mates
% Make sure we have the same number of each
if length(oppind) ~= length(clpind),
   % Have mismatched parentheses
   disp(' ')
   fprintf('Error: Mismatched parenthesis in unit string\n')
   fprintf('Unit string: %s\n',str) ;
   error(' ')
end

% Error check to eliminate multiple characters: /* ** ^^, etc
if ~isempty(opind),
	if min(diff(opind)) == 1,
   	% Have double
	   disp(' ')
	   fprintf('Error: Double operator in unit string\n')
	   fprintf('Unit string: %s\n',str) ;
	   error(' ')
	end
end

% Find number and location of symbols in string
[symbStart,symbEnd]=findrun(isletter(str));
symbNum = length(symbStart);

% Loop through symbols, looking up and putting in first cut 
% at proper symbol exponent.
for i = 1:symbNum,
   
   % Look up symbol
   thisSymb = str(symbStart(i):symbEnd(i));
   [thisFact,thisExp] = lookup(thisSymb);
   
   % Create the unit def list, in proper order
   out(i).symbol = thisSymb;
   
   % See if this symbol is in numerator or denominator
   if (symbStart(i) > divind),
	   out(i).symbolExponent = -1 ;
   else
      out(i).symbolExponent = 1;
   end
   
   % Finish filling out
   out(i).baseFactor   = thisFact;
   out(i).baseExponent = thisExp;
   
   % Take care of any exponents that are directly
   % applied to this symbol.
   if ~isempty(carind),
      thisCarInd = find(symbEnd(i)+1 == carind);
      if ~isempty(thisCarInd),
         % Have an exponent on this symbol
         % Find the numeric string that is the value of this exponent.
         thisCarInd = carind(thisCarInd);
         thisNumInd = find(thisCarInd+1 == numStart);
         thisNum    = str2num(str(numStart(thisNumInd):numEnd(thisNumInd)));
         
         % Apply the exponent
         out(i).symbolExponent = out(i).symbolExponent * thisNum;
      end
   end
end

% Handle parenthesis
% NOT IMPLEMENTED YET