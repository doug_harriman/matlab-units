%UNIT/PRIVATE/APPENDUNIT  Appends the unit definitions of 
%                         one unit object onto another unit object.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [out] = appendunit(in1,in2)

% Loop through unit definitions
out    = in1;
offset = length(in1.unit);
for  i = 1:length(in2.unit),
   % Copy unit definitions in
   out.unit(i+offset) = in2.unit(i);
end
