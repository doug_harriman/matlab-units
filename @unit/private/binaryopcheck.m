%BINARYOPCHECK  Check inputs for unit object binary operators.
%  TF=BINARYOPCHECK(ARG1,ARG2) checks to see if ARG1 and ARG2 have
%  sufficient properties for a unit object binary operation.  Basic checks
%  like unit consistency and matrix size checks are performed.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007 Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [tf] = binaryopcheck(arg1,arg2)

% Basic error check
if nargin ~= 2
    error('Two inputs required.');
end

% Default return value
tf = true;

% Make sure arg2 is a unit.  This function won't get called if arg1 isn't a
% unit object.
if ~isa(arg2,'unit')
    tf = false; %#ok<NASGU>
    error('Second input must be a unit object');
end

% Unit consistency
if ~isconsistent(arg1,arg2)
    tf = false; %#ok<NASGU>
    error('Inputs have inconsistent units.');
end

% Check value sizes.  Allow one to be a scalar.
if ~(isscalar(arg1.value) || isscalar(arg2.value))
    % Neither are scalar, so must be same size.
    if ~all(size(arg1) == size(arg2))
        tf = false; %#ok<NASGU>
        error('Inputs have different sizes.');
    end
end