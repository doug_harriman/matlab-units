%UNIT/MINUS Subraction of one unit object from another.
%           Units must be consistent.  Units of difference
%           will be those of the first number.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = minus(in1,in2)

% Negate and add
in2 = -in2;
out = in1 + in2;