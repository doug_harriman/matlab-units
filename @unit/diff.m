%UNIT/DIFF  Difference for unit objects.
%

% DLH, 12/31/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = diff(in,varargin)

% Just diff the value.
out = in;
out.value = diff(in.value,varargin{:});