%UNIT/MODE   Mode for unit objects.
%

function out = mode(in)

% Error checks
narginchk(1,1);

ustr = char(in);
in = double(in);

out = mode(in);

out = unit(out,ustr);