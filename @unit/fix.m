%UNIT/FIX   FIX for unit objects.
%

% DLH, 11/17/06
% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = fix(in)

ustr = char(in);
val  = double(in);
 
out = unit(fix(val),ustr);