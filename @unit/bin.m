%BIN  Binning function for unit objects.
%  See BIN for help.
%
%  See also: BIN
%

function [centers,count,index] = bin(x,spacing)

% Error checks
error(nargchk(2,2,nargin));

% Handle units
ustr = char(x);
if isa(spacing,'unit')
    spacing = convert(spacing,ustr);
end
[centers,count,index] = bin(double(x),double(spacing));
centers = unit(centers,ustr);