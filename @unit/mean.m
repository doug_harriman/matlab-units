%UNIT/MEAN   Mean for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2011  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = mean(in,dim)

% Error checks
narginchk(1,2);

ustr = char(in);
in = double(in);

switch nargin
    case 1
        out = mean(in);
    case 2
        out = mean(in,dim);
    otherwise 
        error('Unsuppoted number if inputs.');
end

out = unit(out,ustr);