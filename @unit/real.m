%@UNIT/REAL  Return real portion of unit object value.
%

function out = real(in)

% Error checks
error(nargchk(1,1,nargin));

% Get data
out = in;
out.value = real(in.value);
