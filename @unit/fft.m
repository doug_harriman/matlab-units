%UNIT/FFT   FFT for unit objects.
%

% DLH, 01/21/99

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = fft(in)

out = fft(in.value);