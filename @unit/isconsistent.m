%UNIT/ISCONSISTENT  Checks for consistancy of units.
%
%>>isConsistentBoolean = isconsistent(unitObj1, unitObj2)
%

% DLH 11/27/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function iscon = isconsistent(u1,u2)

% Error checks
narginchk(2,2);

% If have strings, create units.
if ischar(u1)
    if isempty(u1)
        iscon = false;
        return;
    end
    
    u1 = unit(1,u1);
end
if ischar(u2)
    if isempty(u2)
        iscon = false;
        return;
    end
    
    u2 = unit(1,u2);
end

if ~isa(u1,'unit')
    iscon = false;
    return;
end
if ~isa(u2,'unit')
    iscon = false;
    return;
end

% Sum up the unit exponents for each object.
u1Exponent = zeros(1,5);
for i = 1:length(u1.unit)
   u1Exponent = u1Exponent + u1.unit(i).symbolExponent*u1.unit(i).baseExponent;
end

u2Exponent = zeros(1,5);
for i = 1:length(u2.unit)
   u2Exponent = u2Exponent + u2.unit(i).symbolExponent*u2.unit(i).baseExponent;
end

% Return equality of the exponents
iscon = min(u1Exponent == u2Exponent);