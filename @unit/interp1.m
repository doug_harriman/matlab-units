%INTERP1  Single dimension interpolation for unit objects.
%   YI = INTERP1(X,Y,XI) interpolates to find YI.
%

function yi = interp1(x,y,xi,varargin)

% Error checks
% Input count
error(nargchk(3,5,nargin));

% Units
if ~isconsistent(x,xi)
    error('Inconsistent X units.');
end

% Convert X to XI units.
x = convert(x,char(xi));

% Do the interpolation
yi = interp1(double(x),double(y),double(xi),varargin{:});

% Assign output units.
if isa(y,'unit')
    yi = unit(yi,char(y));
end
