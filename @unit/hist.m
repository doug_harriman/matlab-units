%UNIT/HIST  Histogram of unit data.
%

function [n,x] = hist(in,buckets)

% Error checks & defaults
error(nargchk(1,2,nargin));
if nargin < 2 
   buckets = 10;
end

% Do histogram
[n,x]=hist(double(in),buckets);

if nargout > 0
    return;
end

% No output args, so do plot.
bar(x,n);
xlabel(['Value (' char(in) ')']);
ylabel('Count'); 