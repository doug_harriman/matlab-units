%UNIT/PROD  Product for unit objects.
%   Note that the unit power will accumulate along the vector.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = prod(in,varargin)

out = in;
out.value = prod(in.value,varargin{:});
out.unit.symbolExponent = out.unit.symbolExponent .* length(in);