%UNIT/MTIMES Matrix multiplication of two unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = mtimes(in1,in2)

% Unit or scalar multiplication
if isa(in1,'unit') && isa(in2,'unit')
   % Multiply the value 
   out       = appendunit(in1,in2);
   out.value = in1.value*in2.value;
   
elseif isa(in1,'unit') && isa(in2,'double')
   % Multiply the value 
   out = in1;
   out.value = in1.value*in2;
   
elseif isa(in1,'double') && isa(in2,'unit')
   % Multiply the value 
   out = in2;
   out.value = in2.value*in1;
   
else
   error('Illegal inputs.');
end

