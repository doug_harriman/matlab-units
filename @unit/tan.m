%TAN   Tangent function for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>


% DLH 
% 06/15/01 - Created

function [out] = ttan(in)

% Convert input to base angular units
in = base(reduce(in));

% Make sure we have radians
str = display(in);
if ~strcmp(str,'rad'),
    error('Must have angular input units');
end

% Do the math
out = tan(double(in));
