%UNIT/TRANSPOSE  Transpose a unit object.
%

function [obj] = transpose(obj)

obj.value = obj.value.';
