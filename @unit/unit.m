%UNIT/UNIT Unit class constuctor
%          Returns a unit object with the given value and
%          units in the string.
%>>unitobj = unit(value,string)
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = unit(varargin)

switch(nargin)
    case(0)
        error('Unit objects require a value and a unit string.');

    case(1)
        % See if we have string or number
        arg = varargin{1} ;
        switch(class(arg))
            case 'unit'
                % Have a unit, so just return it
                out = arg ;
            case 'char'
                % Create a unit with a zero value
                out = unit(0,arg);
            otherwise
                % Error
                error('Unit objects require a value and a unit.');
        end

    case(2)
        % If input is a unit, try to reduce
        if isa(varargin{1},'unit')
            varargin{1} = reduce(varargin{1});
        end
        
        % Error check args
        if ~isnumeric(varargin{1})
            error('Value specification must be numeric.');
        end
        if ~ischar(varargin{2})
            error('Unit specification must be a string.');
        end
        
        % Get the definition of this unit
        try
            uDefs = parse(varargin{2})  ;
        catch err
            throw(err);
        end
        out.unit = uDefs;

        % Convert the given value to base unit value
        out.value = varargin{1};

        if ~isnumeric(out.value),
            error(['Unit value must be numeric.  Input was of type: ' class(out.value)]);
        end

        for i = 1:length(out.unit),
            % Do the conversions for each unit
            out.value = out.value*prod(out.unit(i).baseFactor.^(out.unit(i).symbolExponent)) ;
        end

        % Set class
        out = class(out,'unit') ;

        % Setup heirarchy
        superiorto('double') ;

    otherwise
        error('Too many inputs.');
end