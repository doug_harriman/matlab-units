%COS   Cosine function for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% DLH 
% 06/15/01 - Created

function [out] = cos(in)

% Make sure we have angular units.
try 
    in = convert(in,'rad');
catch
    error('Must have angular input units');
end

% Do the math
out = cos(double(in));
