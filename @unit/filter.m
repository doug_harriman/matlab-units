%UNIT/FILTER  Filter function for unit signals.
%

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>


% DLH, 01/04/99

function out = filter(b,a,in)

% Unwrap and rewrap
out = in;
out.value = filter(b,a,in.value);