%UNIT/LOGLOG  Log plotting for unit objects.
%

% DLH, 30-May-2013

function [han] = loglog(varargin)

% Try to plot new values with consistent units if adding traces.
xunits = [];
yunits = [];
if strcmpi(get(gca,'NextPlot'),'add')
    label = get(get(gca,'ylabel'),'string');
    res   = regexp(label,'\[(?<ustr>.*)\]','names');
    
    % Bail out if multilple units listed.
    if length(res) == 1
        yunits = res.ustr;
        
        % Cleanups
        yunits = strrep(yunits,'\cdot','*');
    end
    
    label = get(get(gca,'xlabel'),'string');
    res   = regexp(label,'\[(?<ustr>.*)\]','names');
    if length(res) == 1
        xunits = res.ustr;
        
        % Cleanups
        xunits = strrep(xunits,'\cdot','*');
    end
end

% Parse out inputs.
switch nargin
    case 1
        x = [];
        y = varargin{1};
        args = {};
    
    case 2
        if ischar(varargin{2})
            % Have single vector and plot format.
            x = [];
            y = varargin{1};
            args = varargin(2);
        else
            % Two plot vectors.
            x = varargin{1};
            y = varargin{2};
            args = {};
        end
        
    otherwise
        % Two plot vectors + arguments.
        x = varargin{1};
        y = varargin{2};
        args = varargin(3:end);
end
   
% Do units conversions if needed
if isa(x,'unit') && isconsistent(x,xunits)
    x = convert(x,xunits);
end

if isa(y,'unit') && isconsistent(y,yunits)
    y = convert(y,yunits);
end

% Do the plotting.
if isempty(x)
    han = loglog(double(y),args{:});
else
    han = loglog(double(x),double(y),args{:});
end
    
% Update the plot labels if needed.
if isempty(xunits) && isa(x,'unit')
    xlabel([ '[' char(x) ']' ]);
end

if isempty(yunits) && isa(y,'unit')
    ylabel([ '[' char(y) ']' ]);
end
    