%UNIT/EQ    Equality check for unit objects
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = eq(in1,in2)

% Make sure both unit objects
if ~isa(in2,'unit')
    if ~isnumeric(in2)
        error('Numeric value expected for input 2.');
    end
    
    %warning('Assuming input 2 is of same units as input 1.');
    in2 = unit(in2,units(in1));
end

if ~binaryopcheck(in1,in2)
    out = 0;
    return;
end

% Return an equality check on the values.
out = in1.value == in2.value;