%UNIT/ISINF   True for infinite elements.
%
%   See also isinf.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = isinf(in)

% Error check inputs
if nargin > 1
    error('One input expected.'); 
end

out = isinf(in.value);