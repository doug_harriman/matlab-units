%UNIT/MEAN   Mean for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2011  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = std(in,flag,dim)

% Error checks
error(nargchk(1,3,nargin));

ustr = char(in);
in = double(in);

switch nargin
    case 1
        out = std(in);
    case 2
        out = std(in,flag);
    case 3
        out = std(in,flag,dim);
    otherwise 
        error('Unsuppoted number if inputs.');
end

out = unit(out,ustr);