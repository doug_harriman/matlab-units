%UNIT/NORM   Norm of a unit object array.
%
%   See also size.
%

% <COPYRIGHT>
% Copyright (c) 1998-2010  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [n] = norm(A,p)

% Error checks
error(nargchk(1,2,nargin));

% Get units
u_str = char(A);
A = double(A);

% Take norm
if nargin == 1
    n = norm(A);
elseif nargin == 2
    n = norm(A,p);
end

% Reapply
n = unit(n,u_str);
