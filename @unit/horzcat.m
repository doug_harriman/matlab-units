%UNIT/HORZCAT  Horizontal concatination of unit objects.
%
%   See also horzcat.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% TODO - Should support concatenation of dissimilar units.

function [out] = horzcat(varargin)

% Make sure all are unit objects
for i = 1:nargin
    if ~isa(varargin{i},'unit')
        error('Objects must be of the type unit.');
    end
end

% Check consistancy
for i = 2:nargin
    if ~isconsistent(varargin{1},varargin{i})
        error('Unit object concatination requires consistent units.');
    end
end

% Concat the values
out  = varargin{1};
rows = size(out,1);
for i = 2:nargin
    if size(varargin{i}.value,1) ~= rows
        error('All inputs must have same number of rows.');
    end
    
    out.value = [out.value varargin{i}.value];
end
