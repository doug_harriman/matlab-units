%UNIT/ROUND  Round value for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = round(in)

out = in;
f   = in.value./double(in);

% Handle zeros
f(in.value==0) = 0;

num = double(in);
num = round(num);

out.value = num.*f;