%UNIT/CUMPROD  Cumulative product for unit objects.
%

% DLH, 12/31/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = cumprod(in,varargin)

error('UMat does not handle vectors of different unit types.');
