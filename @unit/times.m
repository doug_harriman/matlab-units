%UNIT/TIMES Array multiplication of two unit objects.
%

% DLH, 12/31/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = times(in1,in2)

% Error checks
narginchk(2,2);
if (numel(in1) ~= numel(in2)) && ( (numel(in1)~=1) && (numel(in2)~=1) )
    error('Vectors & matrices must be of same size');
end
if size(in1) ~= size(in2)
    in2 = in2';
end

% Unit or scalar multiplication
if isa(in1,'unit') && isa(in2,'unit'),
   % Multiply the value 
   out       = appendunit(in1,in2);
   out.value = in1.value.*in2.value;
   
elseif isa(in1,'unit') && isa(in2,'double'),
   % Multiply the value 
   out = in1;
   out.value = in1.value.*in2;
   
elseif isa(in1,'double') && isa(in2,'unit'),
   % Multiply the value 
   out = in2;
   out.value = in2.value.*in1;
   
else
   error('Illegal inputs.');
end

