%UNIT/MIN   Minimum for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = min(in1,in2)

switch (nargin)
    case (1)
        out = in1;
        out.value = min(in1.value);

    case (2)
        ustr = char(in1);
        in2 = convert(in2,ustr);
        out = unit(min(double(in1),double(in2)),ustr);
        
    otherwise
        error('One or two inputs expected.');
end
