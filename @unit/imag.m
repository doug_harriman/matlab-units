%@UNIT/IMAG  Return imaginary portion of unit object value.
%

function out = imag(in)

% Error checks
error(nargchk(1,1,nargin));

% Get data
out = in;
out.value = imag(in.value);
