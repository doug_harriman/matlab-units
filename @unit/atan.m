%ATAN  Arctangent function for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% DLH 
% 06/15/01 - Created

function [out] = atan(in)

% Convert input to base angular units
in = reduce(in);

% Handle null units, which are ok for angular calcs.
% Assume that null units are radians.
if isa(in,'unit')
    error('Units must cancel');
end

% Do the math
out = unit(atan(double(in)),'rad');
