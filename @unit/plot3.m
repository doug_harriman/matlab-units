%UNIT/PLOT3  Plot3 for unit objects.
%

% Doug Harriman (doug.harriman@gmail.com)
% 03-Mar-2011 - Created

function handle = plot3(x,y,z,varargin)

% Error checks
error(nargchk(3,Inf,nargin));

% Default labels.
xl = '';
yl = '';
zl = '';

% Handle units
if isa(x,'unit')
    xl = ['[' char(x) ']'];
    x  = double(x);
end

if isa(y,'unit')
    yl = ['[' char(y) ']'];
    y  = double(y);
end

if isa(z,'unit')
    zl = ['[' char(z) ']'];
    z  = double(z);
end

% Plot it
handle = plot3(x,y,z,varargin{:});
xlabel(xl);
ylabel(yl);
zlabel(zl);



