%UNIT/CHAR  Create character array of unit object symbol string.
%   STRING=CHAR(UNITOBJ) creates a character array STRING containing the
%   symbols of UNITOBJ.
%
%   Example:
%   >> x = unit(12,'m/sec');
%   >> char(x)
%       x =
%         m/sec
%
%   See also unit/num2str, unit/double.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% TODO - Support output in Matlab & UCUM syntax via user pref.

function [str] = char(unitobj)

% Error checks
if nargin > 1
    error('Too many input arguments.');
end
if nargout > 1
    error('Too many output arguments.');
end

% Build the strings
numerator   = '' ;
denominator = '' ;

% Loop through unit definitions
for i = 1:length(unitobj.unit)

    % Convert this unit to a string
    str = exponent_string(unitobj.unit(i).symbol,unitobj.unit(i).symbolExponent) ;

    % See if it's numerator or denominator
    if unitobj.unit(i).symbolExponent >= 0
        % Numerator
        numerator   = [numerator str] ; %#ok<AGROW>
    else
        % Denominator
        denominator = [denominator str] ; %#ok<AGROW>
    end

end

% Single line numerator and denominators should have '*' between values and
% enclose multiple symbols in parenthesis
numerator   = deblank(numerator);
denominator = deblank(denominator);
if ~isempty(numerator)
    ind = find(numerator == ' ');
else
    ind = [];
end

if ~isempty(ind)
    numerator(ind) = '*';
    %      numerator = ['(' numerator ')'];
end

if ~isempty(denominator)
    ind = find(denominator == ' ');
else
    ind = [];
end

if ~isempty(ind),
    denominator(ind) = '*';
end

% Get rid of trailing blanks
numerator   = deblank(numerator)   ;
denominator = deblank(denominator) ;

% See if we have a denominator
if ~isempty(denominator)
    % Have a denominator
    % Create dividing line
    divider = '-' * ones(1,max(length(numerator),length(denominator))) ;
    divider = char(divider) ;

    % See if we have 0 numerator terms
    if isempty(numerator) && ~isempty(denominator)
        % Insert a one for the numerator
        len = length(divider) ;
        numerator = blanks(len) ;
        numerator(ceil(len/2)) = '1' ;
    end

    % Create unitobj string
    unitobj_string = [deblank(numerator) '/' denominator];

else
    % No denominator
    unitobj_string = numerator ;
end

% Write output
str = strtrim(unitobj_string);


%--------------------------------------------------
% Internal functions
%--------------------------------------------------

%exponent_string
%This function converts a symbol to a single or multi
%line string with the appropriate exponent.  Multiline
%strings are returned as string matrices.
function str = exponent_string(symbol,value)

% Make sure value is positive
value = abs(value);

% If exponent is one, don't print an exponent value.
if value ~= 1,
    str = [symbol '^' num2str(value) ' '] ;
else
    str = [symbol ' '];
end
