
%ROUNDDOWN  Rounds the value up to the specified value.
%  ROUNDVAL=ROUNDDOWN(VALUE,PLACE) rounds the given value to the specified
%  PLACE.  
%
%  PLACE may be any value.  
%  Examples:
%  >> rounddown(12.6,0.25) -> 12.5
%  >> rounddown(1.33,1)    -> 1
%  >> rounddown(1.33,5)    -> 0
%

function out = rounddown(val,place)

% Error checks
error(nargchk(2,2,nargin));
if ~isa(place,'unit')
    error('PLACE must be specfied as a unit value.');
end

% Convert the val to the units of place.
val  = convert(val,place);

% Store out and convert to double.
ustr = char(val);
val = double(val);
place = double(place);

% Do the rounding and package back up.
out = rounddown(val,place);
out = unit(out,ustr);