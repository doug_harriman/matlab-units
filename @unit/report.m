%REPORT  Generates a variable name and value string.
%  STR=REPORT(VAR) returns a string.
%  REPORT(VAR) displays the string to the Matlab command window.
%  REPORT(VAR,FORMAT) formats numeric values with the optional format
%  string.
%

% Doug Harriman (dharriman@xzeres.com)

function str = report(obj,format)

% Default
if nargin < 2
    % General floating point format
    format = '%f';
end

% Build output string
valstr = num2str(obj,format);
if ischar(valstr)
    str = [inputname(1) ' = ' valstr];
elseif iscell(valstr)
    CR = char(10);
    str = [inputname(1) ' = ' CR];
    for i = 1:length(valstr)
        str = [str '  ' valstr{i} CR];
    end
else
    error(['Unhandled type: ' class(valstr)]);
end
    
if nargout > 0
    return;
end

% Just display
disp(str);
clear('str');