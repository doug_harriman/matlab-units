%UNIT/SUBSREF  Unit object subscripted assignment.
%

% DLH, 12/20/98


% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = subsref(A,S)

% Loop through all references
for i = 1:length(S)
   % Switch on reference type
   switch S(i).type
   case '()'
      % Copy over data, then subref the value
      out = A;
      out.value = subsref(A.value,S);
   case '{}'
   	error('Cell references not currently supported.');   
   case '.'
      error('Structure references not currently supported.');
   otherwise
      error('Sub-reference type not recognized.');
   end
   
end
