%UNIT/NUM2STR   Converts number to string with unit symbols.
%
%   See also unit/char, unit/double.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% TODO - Support input precision & format like normal NUM2STR.
% TODO - Support user defined characters to offset unit string.
%        ex: [m], (m), {m}, <m>, etc.
%        Set by passing in a two element character array: '[]','()', etc.

function [str] = num2str(obj,fmt)

% Unit string offset chars
offset_char = '[]';

% Error check inputs
if nargin > 2
    error('Too many input arguments.');
end
if nargout > 2
    error('Too many output arguments.');
end
    
% Convert numeric and character portions
label = [' ' offset_char(1) char(obj) offset_char(2)];
val = double(obj);
for i = 1:numel(val)
    if nargin < 2
        str{i,1} = [num2str(val(i)) label];
    else
        str{i,1} = [num2str(val(i),fmt) label];  % Use number formatting.
    end
end

if numel(val) == 1
    str = str{1};
end