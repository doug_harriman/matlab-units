%UNIT/LE    Less than or equal to operator for unit objects.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = le(in1,in2)

% Make sure both unit objects
if ~isa(in1,'unit') || ~isa(in2,'unit')
   error('Both inputs must be unit objects.');
end

if ~binaryopcheck(in1,in2)
    out = 0;
    return;
end

% Return less than or equal to check on the values.
out = in1.value <= in2.value;