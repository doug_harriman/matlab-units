%UNITS  Returns the units string of a units object.
%

function str = units(obj)

str = char(obj);