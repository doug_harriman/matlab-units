%UNIT/PLUS  Addition of two unit objects.
%           Units must be consistent.  Units of sum
%           will be those of the first number.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = plus(in1,in2)

% Allow NaN's to pass through without error per normal Matlab calcs.
if isnan(in1)
    in1 = unit(NaN,char(in2));
end
if isnan(in2)
    in2 = unit(NaN,char(in1));
end

% Make sure both unit objects
if ~isa(in1,'unit') || ~isa(in2,'unit')
   error('Object must be of the same class.');
end

% Check consistancy
if ~isconsistent(in1,in2)
   error('Unit object addition requires consistent units.');
end

% Add and return
% in2.value = reshape(in2.value,size(in1.value));
out       = in1;
out.value = out.value + in2.value;