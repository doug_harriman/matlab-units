%UNIT/POWER  Unit object array power.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = power(in,val)

% Check for special case: val=0
if val == 0,
   out = 1;
   return;   
end

% Convert the value
out = in;
out.value = in.value.^val;

% Convert the unit
for i = 1:length(out.unit),
   out.unit(i).symbolExponent = out.unit(i).symbolExponent * val;
end

