%SORT  Sort unit object.
%  See HELP SORT.

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [y,ndx] = sort(x,dim)

y = x;
if nargin == 2
    [y.value,ndx] = sort(x.value,dim);
else
    [y.value,ndx] = sort(x.value);
end