%UNIT/ISNAN   True for Not-a-number.
%
%   See also isnan.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = isnan(in)

% Error check inputs
if nargin > 1
    error('One input expected.'); 
end

out = isnan(in.value);