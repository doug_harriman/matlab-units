%SIGFIG  Rounds a unit value to the specified number of significant figures.
%   Y=SIGFIG(X,DIGITS) 
%

function y = sigfig(x,digits)

% Error checks.
error(nargchk(1,2,nargin));

% Defaults
if nargin < 2
    digits = 3;
end

% Cache string.
str = char(x);
x = double(x);
y = sigfig(x,digits);
y = unit(y,str);
