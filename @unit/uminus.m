%UNIT/UMINUS  Negation of unit object.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = uminus(in)

% Negate and add
out       = in;
out.value = -out.value;