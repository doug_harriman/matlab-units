%REDUCE  Cancels and consolidates units.  REDUCE will
%        consolidate units of the same type to the first
%        unit of that type. 
%         
%        Ex:  reduce(unit(1,'m*mm')) ->  .001 m^2
%

% DLH, 12/19/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [out] = reduce(in)

% Convert 'in' to a struct
in = struct(in);

% Loop through the units definitions adding up powers
totalExponent = zeros(1,length(in.unit(1).baseExponent));
for i = 1:length(in.unit),
   % Multiply symbolExponent by the baseExponents and accumulate
   totalExponent = totalExponent + in.unit(i).symbolExponent * in.unit(i).baseExponent;
end

% Build out with base units
out =       unit(1,'m')  ^totalExponent(1);
out = out * unit(1,'g')  ^totalExponent(2);
out = out * unit(1,'sec')^totalExponent(3);
out = out * unit(1,'C')  ^totalExponent(4);
out = out * unit(1,'rad')^totalExponent(5);

% Set the right value
out = out * in.value;

% function [out] = reduce(in)
% 
% % Loop through each unit definition, looking for like 
% % units further down the definition list.
% numUnits = length(in.unit); 
% removeMe = zeros(1,numUnits);  % Index of units to remove.
% for i = 1:numUnits-1,
%    
%    % Get the exponent signature that we're looking for
%    thisExp = in.unit(i).baseExponent;
%    
%    % Loop through the rest of the units looking for a 
%    % matching signature.
%    for j = i+1:numUnits,
%       % Check 'em
%       if min(thisExp == in.unit(j).baseExponent) == 1,
%          % Have a match, change this.symbolExponent
%          in.unit(i).symbolExponent = in.unit(i).symbolExponent + in.unit(j).symbolExponent;
%          
%          % Mark the matching unit for removal
%          removeMe(j) = 1;
%          
%          % See if we've cancelled this unit completely
%          if in.unit(i).symbolExponent == 0,
%             % We have, so mark it for removal too.
%             removeMe(i) = 1;
%          end
%       end
%    end
% end
% 
% % Eliminate all of the units that should be gone.
% out      = in;
% out.unit = in.unit(~removeMe);
