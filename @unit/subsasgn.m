%UNIT/SUBSASGN  Unit object subscripted assignment.
%

% DLH, 12/20/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = subsasgn(A,S,B)

% Loop through all references
for i = 1:length(S),
   % Switch on reference type
   switch S(i).type
   case '()'
      % See if we're attempting to write in a 
      % unitless or unit.
      if isa(B,'unit'),
         % Make sure the units are consistent
         if ~isconsistent(A,B) && ~isempty(A)
            error('Inconsistent units.');
         end
         
         % Assign the reference
   	   A.value(S(i).subs{1}) = B.value;
         
      elseif isa(B,'double'),
	      % Assign the reference
   	   A.value(S(i).subs{1}) = B;
      else,
         error('Attempting to assign an unsupported object to a Unit object.');
      end
   case '{}'
   	error('Cell references not currently supported.');   
   case '.'
      error('Structure references not currently supported.');
   otherwise
      error('Sub-reference type not recognized.');
   end
   
end

% Write the output
out = A;