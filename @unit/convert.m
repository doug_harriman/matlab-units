%UNIT/CONVERT Converts units of number to requested units.
%
%>>unitobj = convert(unitObject,newUnitString)
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

% TODO: Should support a call syntax taking two string values and returning conversion factor.
%       Ex: >> convert('m','in')
%              ans = 39.37

function out = convert(num,des)

% Error checks
narginchk(2,3);

% Allow unit object inputs for units.
if isa(des,'unit')
    des = char(des);
end

% See if it is possible to do the conversion
out = unit(des);
if ~isconsistent(num,out)
   disp(['Original Units: ' char(num)]);
   disp(['New Units     : ' des]);
   error('Can not do conversion, inconsistent units.')
end

% Since the value is stored in the base units, all 
% we have to do to convert is apply the new units.
out.value = num.value;

% If no output, then assign the new units in callers
% workspace.  This saves typing.
% This should be a user selectable behavior, selected
% from a UNITS property page.
if nargout==0
    % Handle intermediate variables
    if ~isempty(inputname(1))
    	% Change value in caller's workspace
       assignin('caller',inputname(1),out) ;
   end
end