%LATEX  LaTeX formats the unit string of a unit object.
%

function str = latex(obj)

% Error checks
error(nargchk(1,1,nargin));

% Convert to a string, and add brackets
str = ['$[' char(obj) ']$'];

% Do LaTeX string conversions.
str = latexmath(str,'latex');
