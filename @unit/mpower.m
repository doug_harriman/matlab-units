%UNIT/MPOWER  Unit object matrix power.
%

% DLH, 12/12/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = mpower(in,val)

% Check for special case: val=0
if val == 0,
   out = 1;
   return;   
end

% Convert the value
out       = in;
out.value = in.value.^val;

% Convert the unit
for i = 1:length(out.unit),
   out.unit(i).symbolExponent = out.unit(i).symbolExponent * val;
end

% Check units
if isa(out,'channel'),
	if isa(out.data,'unit'),
	   out.units = display(out.data,'singleline');
   end
end

