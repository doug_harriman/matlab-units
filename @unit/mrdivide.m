%UNIT/MRDIVIDE   Unit object matrix right divide.
%

% DLH, 12/15/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = mrdivide(in1,in2)

% Just use power and multiply
out = in1 .* power(in2,-1) ;

