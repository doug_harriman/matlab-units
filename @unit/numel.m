%UNIT/NUMEL Number of elements in a unit object.
%
%  See also: numel.
%

function [out] = numel(obj,varargin)

out = numel(obj.value,varargin{:});