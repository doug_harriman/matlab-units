%UNIT/BOBFILT  Bob C's wonderfilter on unit objects.
%

% DLH, 01/04/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function out = bobfilt(in)

% Unwrap, do it, rewrap
out = in;
if nargin == 1
    out.value = bobfilt(in.value);
else
    out.value = bobfilt(in.value,coeff);
end