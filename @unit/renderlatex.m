%RENDERLATEX  Renders unit value and string as a LaTeX string.
%  RENDERLATEX(UNIT_OBJ,FORMAT).  See NUM2STR for FORMAT values.
%

function str = renderlatex(obj,fmt)

% Error checks
error(nargchk(1,2,nargin));

% Generate string value.
if nargin < 2
    str = num2str(obj);
else
    str = num2str(obj,fmt);
end

% Extract unit string.
idx1 = strfind(str,'[');
idx2 = strfind(str,']');

numstr = str(1:idx1-1);
ustr   = str(idx1+1:idx2-1);

% Convert expression types.
% ustr = matexpr2latex(ustr);

% Turn off math formatting for unit string.
str = [numstr ' \mathrm{' ustr '}'];