%ISVALIDUNIT  Determine whether item is a valid unit string.
%

function [tf] = isvalidunit(str)

% Error checks
error(nargchk(1,1,nargin));

if ~ischar(str)
    error('Unit string expected.');
end

% Try to define a simple unit object
tf = true;
try
    temp = unit(1,str); %#ok<NASGU>
    return;
catch %#ok<CTCH>
    tf = false;
end