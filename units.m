%UNITS  Display the GUI unit library.
%

% Valid messages:
% 'create'
% 'hide'
% 'unhide'   
% 'exit'
% 'newunitobject'
% 'newunitdefinition'
% 'newunittype'
% 'deleteunitdefinition'
% 'deleteunittype'
% 'resize'
% 'typeselected'
% 'buildliststring'
% 'loadunitlibrary'
% 'buildtypestring'

% DLH, 01/09/98

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [varargout] = units(msg,varargin)

% Persistent varibles
persistent figHandle         % Library figure handle
persistent unitTypeSelected  % Type user has selected
persistent libarary          % Unit library symbols
persistent libSaveTime       % Time our version was saved.

% No argument cases
if 0 == nargin,
   if isempty(figHandle),
      msg = 'Create';
   else
      msg = 'UnHide';
   end
end

% Switch on message
switch lower(msg)
case 'create'
   % Load INI file
   if exist('units.ini') == 2,
      % Load positions
      load('units.ini');
   else
      % Create positions
      figHeight   = 25.5 ;
      figWidth    = 54.5 ;
      
      set(0,'Units','Characters');
      rootSize = get(0,'ScreenSize');
      figPosition = [(rootSize(3)-figWidth)/2,...
            (rootSize(4)-figHeight)/2,...
            figWidth figHeight];
   end
   
   % Create figure   
   figHandle = figure('Name','Unit Library',...
      'MenuBar','None',...
      'NumberTitle','Off',...
      'Units','Characters',...
      'Resize','Off',...
      'DeleteFcn','units(''Exit'');',...
      'Position',figPosition);   
%      'ResizeFcn','units(''Resize'');',...
%      'CreateFcn','',...
%      'CloseRequestFcn','units(''Exit'');',...
%      'Visible','Off',...
%      'HandleVisiblity','Callback',...

   % Create the menu structure
   mnuOptions = uimenu(figHandle,'Label','&Options');
   mnuNew     = uimenu(mnuOptions,'Label','&New');
   mnuDelete  = uimenu(mnuOptions,'Label','&Delete');
   
   % Create the menus
   uimenu(mnuOptions,'Label','E&xit',...
      'Separator','On',...
      'Callback','units(''Hide'');');
   uimenu(mnuNew,'Label','Unit &Definition',...
      'Callback','units(''NewUnitDefinition'');');
   uimenu(mnuNew,'Label','Unit &Type',...
      'Callback','units(''NewUnitType'');');
   uimenu(mnuDelete,'Label','Unit &Definition',...
      'Callback','units(''DeleteUnitDefinition'');');
   uimenu(mnuDelete,'Label','Unit &Type',...
      'Callback','units(''DeleteUnitType'');');
   
   % Create type selection controls
   uicontrol('Style','Text',...
      'Tag','txtTypes',...
      'Units','Characters',...
      'Position',[0.5 (figHeight-1.6) 11 1.2],...
      'HorizontalAlignment','Left',...
      'String','Unit Type:');
   uicontrol('Style','PopUpMenu',...
      'Tag','popType',...
      'ToolTipString','Unit type to list',...
      'Units','Characters',...
      'Position',[12 (figHeight-2.2) 20 2],...
      'HorizontalAlignment','Left',...
      'Value',1,...
      'String','All');
   
   % Create the list controls
   uicontrol('Style','Text',...
      'Tag','txtList',...
      'Units','Characters',...
      'Position',[0.5 (figHeight-4) 7 1.2],...
      'HorizontalAlignment','Left',...
      'String','Units:');
   uicontrol('Style','ListBox',...
      'Tag','lstList',...
      'ToolTipString','Unit definitions',...
      'Units','Characters',...
      'Position',[8 3 46 20],...
      'HorizontalAlignment','Left',...
      'FontName','Courier',...
      'Value',1,...
      'String',units('BuildListString'));
   
   % Close button
   uicontrol('Style','PushButton',...
      'Tag','btnClose',...
      'ToolTipString','Close window',...
      'Units','Characters',...
      'Position',[(figWidth-7.5) 0.5 7 2],...
      'Callback','units(''Hide'');',...
      'String','Close');
   
   % Show the figure
   set(figHandle,'Visible','On');
   
case 'hide'
   % Make sure handle valid
   if ishandle(figHandle),
      set(figHandle,'Visible','Off');
   end
   
case 'unhide'   
   % Make sure handle valid
   if ishandle(figHandle),
      set(figHandle,'Visible','On');
   else
      units('Create');
   end
   
case 'exit'
   % Clear the stored handle
   figHandle = [] ;
   
   % Save unitTypeLibrary
   % Save INI file
   
case 'newunitobject'
   % Get index of selected unit
   ch  = get(figHandle,'Children');
   ind = get(findobj(ch,'Tag','lstList'),'Value');
   
   % Get data from user
   title=['New ''' deblank(nameLib(ind,:)) ''' Unit Object'];
   prompt={'Variable Name:','Value:'};
   def={'ans','1'};
   lineNo=1;
   answer=inputdlg(prompt,title,lineNo,def);
   
   % Make sure they didn't cancel
   if ~isempty(answer),
      % Try to use
      try
         unitObj = unit(str2num(answer{2}),deblank(symbolLib(ind,:)));
      catch
         error('Illegal unit value.');   
      end
      
      % Assign it in the base workspace
      try 
         assignin('base',answer{1},unitObj);
      catch
         error('Illegal variable name');
      end
   end   
   
case 'newunitdefinition'
   % Define, reload library, and regen list
   defineunit;   
   units('LoadUnitLibrary');
   units('BuildListString');
   
case 'newunittype'
   error(['Not implemented correctly yet:' msg]);   
   
case 'deleteunitdefinition'
   % Get index of selected unit
   ch  = get(figHandle,'Children');
   ind = get(findobj(ch,'Tag','lstList'),'Value');
   
   % Delete that symbol, regen list
   deletefromlibrary(deblank(symbolLib(ind,:)));
   units('LoadUnitLibrary');
   units('BuildListString');
   
case 'deleteunittype'
   error(['Not implemented correctly yet:' msg]);   
case 'resize'   
   error(['Not implemented correctly yet:' msg]);   
case 'typeselected'   
   error(['Not implemented correctly yet:' msg]);   
   
case 'loadunitlibrary'
   % Just load for now, but later check to see if its
   % been updated.
   load library;
   
case 'buildliststring'   
   % See if we need all or just a type
   % Get index of selected unit
   ch  = get(figHandle,'Children');
   han = findobj(ch,'Tag','popType');
   ind = get(han,'Value');
   
   if (1 == ind)
      load library
      fields = fieldnames(library);
      numUnits = length(fields);
      
      str = {};
      for i = 1:numUnits
          field = fields{i};
          str{i} = [field ' ' library.(field).name];
      end
   end
   
   % See if we should output or write
   if (0 == nargout)
      han = findobj(ch,'Tag','lstList');
      set(han,'String',str);
   else
      varargout{1} = str ;
   end
   
case 'buildtypestring'   
   error(['Not implemented correctly yet:' msg]);   
otherwise
   error(['Unknown message: ' msg]);
end

