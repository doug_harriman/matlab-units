%DELETEFROMLIBRARY  Removes a unit definition from the unit database.
%  DELETEFROMLIBRARY(SYMBOL) removes the unit defined by SYMBOL from the
%  units library.
%
% See also addtolibrary, regenlibrary, defineunit, units.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [] = deletefromlibrary(symbol)

% Make sure library exists
if ~exist('library.mat','file'),
   % Regenerate library
   regenlibrary;
end

% Load the unit library
load library;

% Find the index of the symbol
try
    library = rmfield(library,symbol);
catch
    disp(['Symbol: "' symbol '" was not found in the unit library.']);
    return;
end

% Resave library
filePath = which('library.mat');
save(filePath,'library');

% Force lookup to reload file.
clear unit/private/lookup

disp(['Symbol: "' symbol '" removed from the unit library.']);

% EOF
