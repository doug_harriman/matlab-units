%DEFINEUNIT  Define a new unit and add to database.
%  DEFINEUNIT(UNIT_OBJECT,SYMBOL,NAME) defines a new unit identified by
%  SYMBOL as having the value UNIT_OBJECT.  A longer NAME may be provided.
%
%  Example:
%    Define an hour as 60 minutes 
%    >> hour = unit(60,'min');
%    >> defineunit(hour,'hr','Hour')
%
% See also addtolibrary, deletefromlibrary, regenlibrary.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [] = defineunit(unitObj,symbol,name)

% See what calling syntax is used
switch (nargin)
    case (3)
        % Make sure library exists
        if ~exist('library.mat','file'),
            % Regenerate database
            regenlibrary;
        end
        
        % Error check inputs
        if ~isa(unitObj,'unit')
            error('Unit object required.'); 
        end
        if ~isa(symbol,'char')
            error('Text definition for symbol required.'); 
        end
        
        % Default long name
        if nargin < 3
            name = ''; 
        end

        % Loop through component units
        value   = double(unitObj);
        unitObj = struct(unitObj);
        baseFactor   = ones (1,length(unitObj.unit(1).baseFactor));
        baseExponent = zeros(1,length(unitObj.unit(1).baseExponent));
        for i = 1:length(unitObj.unit)
            baseFactor   = baseFactor   * ...
                unitObj.unit(i).baseFactor^unitObj.unit(i).symbolExponent;
            baseExponent = baseExponent + ...
                unitObj.unit(i).baseExponent*unitObj.unit(i).symbolExponent;
        end

        % Do final scaling
        baseFactor = baseFactor * value;

    case (0)
        % GUI
        prompt = {'Unit Value:','Unit definition:','Symbol:','Name:'};
        def    = {'1','','',''};
        title  = 'New Unit Definition';
        lineNo = 1;
        answer = inputdlg(prompt,title,lineNo,def);

        % Exit if user hit cancel
        if isempty(answer)
            return;
        end
        
        % Call DEFINEUINT with 3 args.
        unitObj = unit(str2num(answer{1}),answer{2}); %#ok<ST2NM>
        defineunit(unitObj,answer{3},answer{4});
        return;

    otherwise
        error('Illegal number of parameters for DEFINEUNIT.');
end

% Add it to the library
addtolibrary(symbol,baseFactor,baseExponent,name);

% Output
disp(['Unit "' name '" added to unit library with symbol "' symbol '".']);

