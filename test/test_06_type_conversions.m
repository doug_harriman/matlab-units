%TEST_06_TYPE_CONVERSIONS Test data type conversions for unit objects.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_06_type_conversions

% Default
res = 0;

%CHAR
x = unit(11,'m/sec');
if ~strcmp(char(x),'m/sec')
    res = 1;
    return;
end

%DOUBLE
value = 10.2;
x = unit(value,'in');
if double(x) ~= value
    res = 2;
    return;
end

%NUM2STR
x = unit(12,'m/sec');
if ~strcmp(num2str(x),'12 m/sec')
    res = 3;
    return;
end

%DISP
% No output, just make sure no errors
x = unit (11,'m');
disp(x)

%DISPLAY
% No output, just make sure no errors
x = unit (11,'m');
display(x)

% ISINF
x = unit([1 inf 0],'m');
if ~any(isinf(x))
    res = 4;
    return;
end
x = unit(12,'sec');
if isinf(x)
    res = 5;
    return;
end

% ISNAN
x = unit([1 nan 0],'m');
if ~any(isnan(x))
    res = 6;
    return;
end
x = unit(12,'sec');
if isnan(x)
    res = 7;
    return;
end
