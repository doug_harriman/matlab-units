%TEST_01_CLASS_INSTANTIATION  Tests the creation of unit objects.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_01_class_instantiation

% Default return value
res = 0;

% Basic ways to create an object
% Base units
try
    obj = unit(1,'m');
    obj = unit(7,'g');
    obj = unit(12,'sec');
    obj = unit('C');
    obj = unit(7.5,'rad');
catch
    res = 1;
end
    
% Some basic failure expected creations:
try
    unit(6);
    
    % Should not get here
    res = 2;
    return;
end

try
    unit;
    
    % Should not get here
    res = 3;
    return;
end

try
    unit(6,12);
    
    % Should not get here
    res = 4;
    return;
end

try
    unit(6,'m',3);
    
    % Should not get here
    res = 5;
    return;
end

