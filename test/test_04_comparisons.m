%TEST_04_COMPARISONS  Test comparison operations on unit objects.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_04_comparisons

% Default return value
res = 0;

% Two units of the same dimension, one of another.
m   = unit(1,'m');       % meter
in  = unit(39.37,'in');  % one meter in inches
sec = unit(1,'sec');     % second (time)

% Equality
if ~(m == in)
    res = 1;
    return;
end
try
    m == sec;
    
    % Should not get here
    res = 2;
    return;
end
try 
    m == 1;
    
    % Should not get here
    res = 22;
    return;
end

% Inequality    
if (m ~= in)
    res = 3;
    return;
end
try 
    ~(m ~= sec);
    
    % Should not get here
    res = 4;
    return;
end
try 
    m ~= 1;
    
    % Should not get here
    res = 42;
    return;
end


% Greater than
in = unit(1,'in');
if ~(m > in)
    res = 5;
    return;
end
if (in > m)
    res = 6;
    return;
end
try
    m > sec
    
    % Should not get here
    res = 7;
    return;
end
try 
    m > 1;
    
    % Should not get here
    res = 72;
    return;
end


% Less than
in = unit(1,'in');
if ~(in < m)
    res = 8;
    return;
end
if (m < in)
    res = 9;
    return;
end
try
    m < sec
    
    % Should not get here
    res = 10;
    return;
end
try 
    m < 1;
    
    % Should not get here
    res = 11;
    return;
end

% Greather than or equal
in = unit(1,'in');
if ~(m >= in)
    res = 12;
    return;
end
if (in >= m)
    res = 13;
    return;
end
try
    m >= sec
    
    % Should not get here
    res = 14;
    return;
end
try 
    m >= 1;
    
    % Should not get here
    res = 15;
    return;
end


% Less than or equal
in = unit(1,'in');
if ~(in <= m)
    res = 16;
    return;
end
if (m <= in)
    res = 17;
    return;
end
try
    m <= sec
    
    % Should not get here
    res = 18;
    return;
end
try 
    m <= 1;
    
    % Should not get here
    res = 19;
    return;
end
