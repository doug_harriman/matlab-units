%TEST_03_UNIT_OPS  Basic operations on unit objects, convert, reduce, etc.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_03_unit_ops

% Default return value
res = 0;

% Two units of the same dimension, one of another.
m   = unit(1,'m');   % meter
in  = unit(1,'in');  % inch
sec = unit(1,'sec'); % second (time)

% Convert 'm' to 'in'
x = convert(m,'in');
x = double(x);
if x ~= 39.37
    % Unit conversion incorrect.
    res = 1;
    return;
end

% Convert 'm' to 'sec'
try 
    x = convert(m,'sec');

    % Should not get here
    res = 2;
    return;
end

% Get the base units of inches
x = base(in);
x = double(x);
if x ~= inv(39.37)
    % Wrong base unit value returned
    res = 3;
    return;
end

% Reduce inches to base units.
x = reduce(in);
x = double(x);
if x ~= inv(39.37)
    % Wrong base unit value returned
    res = 4;
    return;
end

% Consistency checks
if ~isconsistent(in,m)
    res = 5;
    return;
end

if isconsistent(m,sec)
    res = 6
    return;
end

