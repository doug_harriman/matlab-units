%TEST_07_SIZE_ATTR Test different object size functions for unit objects.

function [res] = test_07_size_attr

% Default
res = 0;

% SIZE
val = [1 2;3 4; 5 6];
x = unit(val,'m');
sz = size(x);
if ~all(size(sz) == size(size(val)))
    % Should have returned [3 2]
    res = 1;
    return;
end
[r,c] = size(x);
if ~( (r==size(val,1)) && (c==size(val,2)) )
    % Should have returned row and col separately
    res = 2;
    return;
end
sz = size(x,1);
if ~(sz==size(val,1))
    % Should have returned row size
    res = 3;
    return;
end
sz = size(x,2);
if ~(sz==size(val,2))
    % Should have returned col size
    res = 4;
    return;
end

% LENGTH
val = [1 2;3 4; 5 6];
x = unit(val,'m');
if length(x) ~= 3
    res = 5;
    return;
end

% HORZCAT
x = unit(1,'m');
y = [x x x];
if ~all(size(y) == [1 3])
    res = 6
    return;
end
if ~strcmp(char(y),'m')
    res = 7;
    return;
end
try
    % Should fail
    z = [x [x;x]];
    
    % Should not get here
    res = 8;
    return;
end


% VERTCAT
x = unit(1,'m');
y = [x; x; x];
if ~all(size(y) == [3 1])
    res = 9;
    return;
end
if ~strcmp(char(y),'m')
    res = 10;
    return;
end
try
    % Should fail
    z = [x; [x x]];
    
    % Should not get here
    res = 11;
    return;
end
