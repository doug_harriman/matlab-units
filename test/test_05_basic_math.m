%TEST_05_BASIC_MATH  Test basic unit object math operations.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_05_basic_math

% Default return value
res = 0;

% ABS
x = unit(-12,'m');
y = abs(x);
if x == y
    res = 1;
    return;
end

% ROUND
x = unit(12.7,'m');
y = round(x);
if x == y
    res = 2;
    return;
end

% FIX
x = unit(12.7,'m');
y = round(x);
if x == y
    res = 3;
    return;
end

% SIGN
x = unit(-2,'m');
y = sign(x);
if y ~= -1
    res = 4;
    return;
end

% MAX
m  = unit(1,'m');
in = unit(1,'in');
z  = max(m,in);
if z ~= m
    res = 5;
    return;
end

% MIN
z  = min(m,in);
if z ~= in
    res = 5;
    return;
end

% PLUS
minute = unit(1,'min');
sec = unit(1,'sec');
z = minute + sec;
if z ~= unit(60+1,'sec')
    res = 6;
    return;
end

% UPLUS
z = +in;
if z ~= in
    res = 7;
    return;
end

% SUM
x = unit([1 2 3],'in');
z = sum(x);
if z ~= unit(6,'in')
    res = 8;
    return;
end

% CUMSUM
% TODO - CUMSUM test easiest if ALL or ANY work for units.

% UMINUS
z = -in;
if z >= in
    res = 9;
    return;
end

% MINUS
z = minute - sec;
if z ~= unit(60-1,'sec')
    res = 10;
    return;
end

% MTIMES
if (2*sec) ~= unit(2,'sec')
    res = 11;
    return;
end
if (sec*2) ~= unit(2,'sec')
    res = 12;
    return;
end
if (sec*sec) ~= unit(1,'sec^2')
    res = 13;
    return;
end

% PROD
x = unit([1 2 3],'in');
z = prod(x);
if z ~= unit(6,'in^3')
    res = 14;
    return;
end

% TODO - MRDIVIDE

% RDIVIDE
z = minute/sec;
z = reduce(z);
if z ~= 60
    res = 15;
    return;
end

% POWER
z = unit(2,'sec')^2;
if z ~= unit(4,'sec^2')
    res = 16;
    return;
end

% TODO - MPOWER

% INV
x = unit(2,'m');
y = inv(x);
z = x*y;
z = reduce(z);
if z ~= 1
    res = 17;
    return;
end

% SQRT
z = sqrt(unit(4,'sec^2'));
if z ~= unit(2,'sec')
    res = 18;
    return;
end

