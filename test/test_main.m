%TEST_MAIN  Main test harness 
%  TEST_MAIN runs all other files in the test directory.  Each file is
%  expected to return zero on success.  Any non-zero value is interpreted
%  as a test failure.  Interpretation of the non-zero return code is up to
%  the writer of the individual test file.  Errors in sub-test files are
%  caught and interpreted as test failures.
%

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_main

% Find all files in the directory with TEST_MAIN.M
thisfile = mfilename;
path     = fileparts(which(mfilename));
d        = dir([path filesep '*.m']);

disp(' ');
disp('Running unit tests');

n_test = length(d);  % Skip this file to avoid recursive loop
res    = zeros(n_test,1);
for i_test = 1:n_test,
    [fpath,file] = fileparts(d(i_test).name);

    % Skip this file
    if ~strcmp(file,thisfile)
        fprintf('\nRunning test: %s...',file);

        try
            % Run the test file.  Can do something with the result.
            res(i_test) = feval(file);
            fprintf('success');
        catch
            % Call failed, so need to report it.
            res(i_test) = 1;
            fprintf('failure');
            fprintf('\nerror: %s',lasterr);
        end
    end
end

fprintf('\n');

if any(res)
    disp(['Test failed: ' num2str(nnz(res)) '/' num2str(length(res))]);
else
    disp('Test succeeded');
end

