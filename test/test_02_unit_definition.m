%TEST_02_UNIT_DEFINITION  Defines, looks up and deletes a new unit defintion.

% <COPYRIGHT>
% Copyright (c) 1998-2007  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [res] = test_02_unit_definition

% Default return value
res = 0;
test_unit = 'woobie';

% Backup the library to see if we can regenerate it
libfile = 'library.mat';
if exist(libfile) == 2
    % Find the file 
    path = fileparts(which('defineunit'));
    fn   = fullfile(path,libfile);
    
    % Backup the file and remove the original copy
    if ispc
        copycmd = 'copy';
    elseif isunix
        copycmd = 'cp';
    else
        error('Unknown system type');
    end
    status = system([copycmd ' ' fn ' ' fn '.bak']);
    if status ~= 0
        error('Unable to make backup file of library.');
    end
    
    delete(fn);
    
    % Try to regen the library
    try
        regenlibrary;
    catch
        % If that failed, restore orginal library and exit with failure
        status = system([copycmd ' ' fn '.bak ' fn]);
        if status ~= 0
            error('Unable to restore library from backup file.');
        end
        
        res = 1;
        return;
    end

    % Delete newly generated library and copy backup back.
    delete(fn);
    status = system([copycmd ' ' fn '.bak ' fn]);
    if status ~= 0
        error('Unable to restore library from backup file.');
    end
    
else
    % File doesn't exist so just generate the library
    regenlibrary;
end


% Try to define an already existing unit, should fail.
% Make sure that the test unit doesn't exist.
try
    unit(1,test_unit);
    
    % Should fail.
    res = 2;
    return;
end

% Define the new unit.
defineunit(unit(1.2,'m'),test_unit,'Test Unit');

% Instantiate a new unit object.
x = unit(1,test_unit);

% Delete the new unit.
deletefromlibrary(test_unit);