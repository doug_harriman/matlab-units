UCUM Units Toolbox for Matlab

Display of unit objects.
DISP    - Displays just the value of a unit object.  Calls NUM2STR to convert the data.
DISPLAY - Displays the variable name and value of a unit object.  Calls DISP.

Heirarch of calls for display:
DISPLAY
   |
 DISP
   |
NUM2STR
   /\
  /  \
 /    \
DOUBLE CHAR


Data type conversions

CHAR - Returns character part of a unit object.  This is the symbol string for a unit object.

Example:
>> x = unit(10,'in');
>> char(x)
   x = 'in'

DOUBLE - Returns the numeric value of the unit object in its current units.  Example: 
>> x = unit(10,'in');
>> y = double(x)
   y = 10

>> z = convert(x,'m')
   z = 0.254 m
>> y = double(z)
   y = 0.254

NUM2STR - Convert unit object to a string value for printing.  Essentially the same as calling both DOUBLE and CHAR to return a character value showing both value and units.

Example:

>> x = unit(2,'m');
>> num2str(x)
   ans = '2 m'
