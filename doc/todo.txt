---------------------------
Preferences to add
---------------------------
- Toolbox Version
- Pass by ref or value.  Affects whether things like convert will affect the original object or not.  
  Pass by value example:
  >> x = unit(1,'m');
  >> convert(x,'cm')
     ans = 100 cm
  >> x
     ans = 1 m

  Pass by reference example:
  >> x = unit(1,'m');
  >> convert(x,'cm')
     ans = 100 cm
  >> x
     x = 100 cm
  Default to pass by value, because it is the default Matlab behavior.

- UCUM strict mode.  
  Doesn't allow usage of non-UCUM units or definition of values.

- Unit string parsing format: UCUM or Matlab.
  Should default to Matlab (though XML read must use UCUM).

- 24-Sep-09.  Just allowed comparisons (<,>,==) with non-unit numeric values.
  Wanted to allow checks like x > 0, without having to make 0 a unit object.
  Should featurize to give 3 modes:
  - Strict: both values must be units.
  - Loose:  second value can be any numeric value (current behavior)
  - Zero:   Only supports non-unit comparisons if the second value is zero.     

---------------------------
Other
---------------------------
- Completely rework library/database read/write and lookup structure.  
- Add error_id to all ERROR calls.
- Need more advanced parser for Matlab syntax.
- Need a parser for the UCUM syntax
- TeX output format.  TEX or TEXSTR for command name.
- Arrays only displayed as row vectors.
- Need a transpose command.


---------------------------
Tests to complete
---------------------------
test_05_basic_math.m
test_06_type_conversions.m
test_07_size_attr.m
test_08_adv_math.m
test_main.m

---------------------------
Needed @unit files
---------------------------
acos
asin
any 
all
times (element wise multiplication)
ldivide (left elementwise division ???)
mldivide (matrix left division)
mpower (matrix power)
