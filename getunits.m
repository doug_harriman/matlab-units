%GETUNITS  Extracts units from a string.
%  
% >> str  = 'Air Speed of a Swallow [m/sec]';
% >> ustr = getunits(str)
%   ans = 'm/sec'
%

function ustr = getunits(str)

% Error checks
error(nargchk(1,1,nargin));
if ~ischar(str)
  error('character input expected.');
end
if size(str,1) ~= 1
  error('row vector expected.');
end

% Default value.
ustr = [];

% Look for '[]'
a = strfind(str,'[');
b = strfind(str,']');

if isvalid(a,b)
  ustr = str(a+1:b-1);
  return;
end

% Look for '()'
a = strfind(str,'(');
b = strfind(str,')');

if isvalid(a,b)
  ustr = str(a+1:b-1);
  return;
end

function tf = isvalid(a,b)

tf = false;

if isempty(a)
  return;
end

if isempty(b)
  return;
end

if length(a) ~= length(b)
  return;
end

if length(a) ~= 1
  return;
end

tf = true;
