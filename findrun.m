%FINDRUN  Identifies the starting and ending indecies of a string of
%         ones in boolean arrays.
%
%>>[start,end,length] = findrun(boolArray)
%

% DLH
% 11/29/98 - Created
% 09/11/02 - Added minumum length specifier

% <COPYRIGHT>
% Copyright (c) 1998-2006  Doug Harriman
% </COPYRIGHT>
% <LICENSE>
% See LICENSE file in distribution for licensing details of this source file
% </LICENSE>

function [start,stop,len] = findrun(data,min_length)

% Error check input
% Values
if ~isbool(data) || min(size(data))~=1
   error('''findrun'' works only with boolean vectors.');
end
% Size
[r,c]=size(data);
if (r>c), data=data'; end

% Prepad with 0 and postpad so start indecies work out right.
data = [0 data 0];

% Use diff to identify runs
diffData = diff(data);

% Get indecies
start = find(diffData ==  1);
stop  = find(diffData == -1) - 1 ;
len   = stop-start+1;

% Col vectors
start = start';
stop  = stop';

% Bail if no min length specified
if nargin < 2,
    return;
end

% Length of runs
ind = len >= min_length;
start = start(ind);
stop  = stop(ind);

